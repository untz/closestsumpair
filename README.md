# Find a pair in sorted (ascending) array whose sum is closest to the sum parameter.

## Specification

Example1:

Input: ```arr[] = {10, 22, 28, 29, 30, 40}, sum = 54```

Output: ```[22, 30]```

---

Example2: 

Input: ```arr[] = {1, 3, 4, 7, 10}, sum = 15```

Output: ```[4, 10]```

---

### Acceptance Criteria

* Must return closest paired values based on sum.

* Improper parameter inputs (array must not be null & sum must not be less than one)
must throw specific NullPointerException and IllegalArgumentException.

* Must be testable via JUnit (full code coverage, especially the NullPointerException and IllegalArgumentException) and run via a Maven build script.

---

### System Requirements

Need a computer with Java 1.8 (whether its the JDK or JRE) installed along with Apache Maven 3.3.3 set to run from the command line.

---

#### How to build and run the project

First way, is directly from command line, go to the root of the project and type:

`mvn clean install` (this runs the unit tests and will print out the results to stdout).