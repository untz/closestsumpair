package com.puzzles;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ClosestSumPairTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void closestPair_whenSumIs54() {
        int[] arr = new int[] {10, 22, 28, 29, 30, 40};
        int[] closestPair = ClosestSumPair.closestPairBasedOnSum(arr, 54);
    }

    @Test
    public void closestPair_whenSumIs15() {
        int[] arr = new int[] {1, 3, 4, 7, 10};
        int[] closestPair = ClosestSumPair.closestPairBasedOnSum(arr, 15);
    }

    @Test
    public void closestPair_whenArrayIsNull() {
        int[] arr = null;
        exception.expect(NullPointerException.class);
        int[] closestPair = ClosestSumPair.closestPairBasedOnSum(arr, 54);
    }

    @Test
    public void closestPair_whenSumIsLessThanOne() {
        int[] arr = new int[] {10, 22, 28, 29, 30, 40};
        exception.expect(IllegalArgumentException.class);
        int[] closestPair = ClosestSumPair.closestPairBasedOnSum(arr, -10);
    }
}