package com.puzzles;

import java.util.Arrays;

public class ClosestSumPair {

    public static int[] closestPairBasedOnSum(int[] arr, int sum) {
        if (arr == null) {
            throw new NullPointerException("arr cannot be null");
        }

        if (sum < 1) {
            throw new IllegalArgumentException("sum cannot be less than 1");
        }

        int[] closestPair = new int[2];

        int left = 0;
        int right = arr.length - 1;
        int diff = Integer.MAX_VALUE;

        while (right > left) {
            if (Math.abs(arr[left] + arr[right] - sum) < diff) {
                closestPair[0] = arr[left];
                closestPair[1] = arr[right];
                diff = Math.abs(arr[left] + arr[right] - sum);
            }

            if (arr[left] + arr[right] > sum) {
                right--;
            }
            else {
                left++;
            }
        }
        return closestPair;
    }

    public static void main(String[] args) {
        int [] arr = new int[] {10, 22, 28, 29, 30, 40};
        int [] closestPair = ClosestSumPair.closestPairBasedOnSum(arr, 54);
        System.out.println(Arrays.toString(closestPair));

        int [] arr2 = new int[] {1, 3, 4, 7, 10};
        int [] closestPair2 = ClosestSumPair.closestPairBasedOnSum(arr2, 15);
        System.out.println(Arrays.toString(closestPair2));
    }
}